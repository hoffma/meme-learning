#include <iostream>
#include <vector>
#include <cstdlib>
#include <cmath>

using namespace std;

struct Gate {
  float a;
  float b;
  float y;
};

std::vector<Gate> and_gate {
  {0, 0, 0},
  {0, 1, 0},
  {1, 0, 0},
  {1, 1, 1}
};

std::vector<Gate> or_gate {
  {0, 0, 0},
  {0, 1, 1},
  {1, 0, 1},
  {1, 1, 1}
};

std::vector<Gate> nand_gate {
  {0, 0, 1},
  {0, 1, 1},
  {1, 0, 1},
  {1, 1, 0}
};

std::vector<Gate> nor_gate {
  {0, 0, 1},
  {0, 1, 0},
  {1, 0, 0},
  {1, 1, 0}
};


const std::vector<Gate> &train_data = and_gate;

float sigmoid_f(float t) {
  return (1.f)/(1.f+expf(-t));
}

float random_float() {
  uint64_t r = std::rand();

  return static_cast<float>(r)/static_cast<float>(RAND_MAX);
}

float loss(float w1, float w2, std::vector<Gate> data, float bias) {
  float res = 0;

  for(const auto &dp : data) {
    auto a = dp.a * w1;
    auto b = dp.b * w2;
    auto y = sigmoid_f(a + b + bias);
    auto err = (y - dp.y)*(y - dp.y);
    /*cout << "x: " << x << ", y: " << y << ", expected: " << dp.expected << "\n";*/
    /*cout << "err: " << err << "\n";*/
    res += err;
  }
  res /= data.size();

  return res;
}

int main() {
  /*std::srand(time(nullptr));*/
  std::srand(69);
  constexpr float eps = 1e-2;
  constexpr float rate = 1e-1;

  auto w_a = random_float();
  auto w_b = random_float();
  auto bias = random_float();
  auto l = loss(w_a, w_b, train_data, bias);
  /*while (l > 0.1f) {*/
  cout << "w_a: " << w_a << ", w_b: " << w_b << ", b: " << bias << ", loss: " <<  l << "\n";
  for (int i = 0; i < 100; ++i) {
  /*cout << "w_a: " << w_a << ", w_b: " << w_b << ", loss: " <<  l << ", b: " << bias << "\n";*/

    float d1 = (loss((w_a+eps), (w_b), train_data, bias) - l)/(eps);
    float d2 = (loss((w_a), (w_b+eps), train_data, bias) - l)/(eps);
    float db = (loss((w_a), (w_b), train_data, bias+eps) - l)/(eps);

    w_a -= rate*d1;
    w_b -= rate*d2;
    bias -= rate*db;
    l = loss(w_a, w_b, train_data, bias);
  }
  cout << "w_a: " << w_a << ", w_b: " << w_b << ", b: " << bias << ", loss: " <<  l << "\n";

  for(int i = 0; i < 2; ++i) {
    for (int j = 0; j < 2; ++j) {
      cout << i << " | " << j << " || " << sigmoid_f(w_a*i + w_b*j + bias) << "\n"; 
    }
  }

  return 0;
}
