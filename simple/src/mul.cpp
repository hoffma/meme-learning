#include <iostream>
#include <vector>
#include <cstdlib>

using namespace std;

struct DataPoint {
  int32_t input;
  int32_t expected;
};

struct Gate {
  uint8_t a;
  uint8_t b;
  uint8_t y;
};

std::vector<DataPoint> times_three {
  {0, 0},
  {1, 3},
  {2, 6},
  {3, 9},
  {4, 12},
  {5, 15},
  {6, 18}
};

std::vector<DataPoint> times_five {
  {0, 0},
  {1, 5},
  {2, 10},
  {3, 15},
  {4, 20},
  {5, 25},
  {6, 30}
};

const std::vector<DataPoint> &train_data = times_five;

float random_float() {
  uint64_t r = std::rand();

  return static_cast<float>(r)/static_cast<float>(RAND_MAX);
}

float loss(float w, std::vector<DataPoint> data) {
  float res = 0;

  for(const auto &dp : data) {
    auto x = dp.input;
    auto y = x*w;
    auto err = (y - dp.expected)*(y - dp.expected);
    /*cout << "x: " << x << ", y: " << y << ", expected: " << dp.expected << "\n";*/
    res += err;
  }
  res /= data.size();

  return res;
}

int32_t eval(int32_t number, float w) {
  return static_cast<int32_t>(static_cast<float>(number)*w);
}

int main() {
  std::srand(time(nullptr));
  auto cnt = 0;
  constexpr float eps = 1e-3;
  constexpr float rate = 1e-3;

  auto w = random_float()*10.f;
  auto l = loss(w, train_data);
  while (l > 0.1f) {
    cnt++;
    cout << "weight: " << w << ", loss: " <<  l << "\n";

    float derive = (loss((w+eps), train_data) - l)/(eps);
    w -= rate*derive;
    l = loss(w, train_data);
  }
  cout << "weight: " << w << ", loss: " <<  l << ", cnt: " << cnt << "\n";

  cout << "evaluating:\n";
  for (int i = 10; i < 20; ++i) {
    cout << "f(" << i << ") = " << eval(i, w) << "\n";
  }

  return 0;
}
