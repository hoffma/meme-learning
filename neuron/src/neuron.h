#pragma once
#include <array>
#include <functional>
#include <vector>
#include <cmath>
#include <ostream>

struct Neuron {
  Neuron(std::function<float()> weight_f, 
      std::function<float()> bias_f, std::function<float(float)> activation_f)
      : activation(activation_f) {

    for(auto &w : weights) {
      w = weight_f();
    }
    bias = bias_f();
  }

  Neuron(std::array<float, 2> w, float b) : weights(w), bias(b) {}

  float eval(std::array<float, 2> input) {
    float sum = 0.f;
    
    for (auto i = 0; i < input.size(); ++i) {
      sum += input[i]*weights[i];
    }
    return activation(sum + bias);
  }

  std::array<float, 2>& get_weights() {
    return weights;
  }

  void set_weights(std::array<float, 2> w) {
    weights = w;
  }

  float get_bias() { return bias; }
  void set_bias(float b) { bias = b; }

  friend std::ostream& operator<<(std::ostream& os, Neuron const& n);

  float output;
  std::array<float, 2> weights;
  std::function<float (float)> activation;
  float bias;
};

