#include "neuron.h"

std::ostream &operator<<(std::ostream &os, Neuron const& n) {
  os << "{";
  for (int i = 0; i < n.weights.size(); ++i) {
    os << "w" << i << ": " << n.weights[i] << ", ";
  }
  os << "b: "<< n.bias << "}";
  return os;
}
