#include <iostream>
#include <cmath>
#include <vector>
#include <algorithm>
#include "neuron.h"

using namespace std;

struct Gate {
  /*std::array<float, 2> inp;*/
  float a;
  float b;
  float y;
};

std::vector<Gate> and_data {
  {0, 0, 0},
  {0, 1, 0},
  {1, 0, 0},
  {1, 1, 1},
};

std::vector<Gate> or_data {
  {0, 0, 0},
  {0, 1, 1},
  {1, 0, 1},
  {1, 1, 1},
};

std::vector<Gate> nor_data {
  {0, 0, 1},
  {0, 1, 0},
  {1, 0, 0},
  {1, 1, 0},
};

std::vector<Gate> nand_data {
  {0, 0, 1},
  {0, 1, 1},
  {1, 0, 1},
  {1, 1, 0},
};

std::vector<Gate> xor_data{
  {0, 0, 0},
  {0, 1, 1},
  {1, 0, 1},
  {1, 1, 0},
};

std::vector<Gate> &training_data = xor_data;

struct XorNet {
  XorNet(std::function<float()> wf, std::function<float()> bf, std::function<float(float)> act)
    : or_n(wf, bf, act), nand_n(wf, bf, act), and_n(wf, bf, act){}
  Neuron or_n;
  Neuron nand_n;
  Neuron and_n;
};

float sigmoid_f(float t) {
  return (1.f)/(1.f+expf(-t));
}

float forward(XorNet &xn, float ai, float bi) {
    auto a = xn.or_n.activation(xn.or_n.weights[0] * ai + xn.or_n.weights[1] * bi + xn.or_n.bias);
    auto b = xn.nand_n.activation(xn.nand_n.weights[0] * ai + xn.nand_n.weights[1] * bi + xn.nand_n.bias);
    auto y = xn.and_n.activation(xn.and_n.weights[0] * a + xn.and_n.weights[1] * b + xn.and_n.bias);
    return y;
}

float cost(XorNet &xn, const std::vector<Gate> &tdata) {
  float res = 0;

  for (const auto &dp : tdata) {
    auto a = dp.a;
    auto b = dp.b;
    auto y = forward(xn, a, b);
    auto err  = (y - dp.y)*(y - dp.y);
    /*std::cout << "err: " << err << "\n";*/
    res += err;
  }
  res /= tdata.size();
  /*std::cout << "res: " << res << "\n";*/

  return res;
}


float cost(Neuron &n, const std::vector<Gate> &tdata) {
  float res = 0;

  for (const auto &dp : tdata) {
    auto a = n.weights[0] * dp.a;
    auto b = n.weights[1] * dp.b;
    auto y = n.activation((a + b + n.bias));
    auto err  = (y - dp.y)*(y - dp.y);
    /*std::cout << "err: " << err << "\n";*/
    res += err;
  }
  res /= tdata.size();

  return res;
}

Neuron learn(float eps, float lr, Neuron n, const std::vector<Gate> &tdata) {
  auto tmp_w = n.get_weights();
  auto tmp_b = n.get_bias();
  std::array<float, 2> dc;
  float init_cost = cost(n, tdata); /* initially calculate loss */

  for (auto i = 0; i < tmp_w.size(); ++i) {
    auto w = n.get_weights();
    w[i] += eps;
    Neuron tmp_n{n};
    tmp_n.set_weights(w);
    auto new_cost = cost(tmp_n, tdata);
    dc[i] = (new_cost - init_cost)/eps;
  }
  auto b = n.get_bias();
  b += eps;
  Neuron tmp_n{n};
  tmp_n.set_bias(b);
  auto new_cost = cost(tmp_n, tdata);
  auto db = (new_cost - init_cost)/eps;

  for (auto i = 0; i < tmp_w.size(); ++i) {
    tmp_w[i] -= lr*dc[i];
  }
  n.set_weights(tmp_w);
  n.set_bias(n.get_bias() - (lr*db));

  return n;
}

XorNet finite_diff(float eps, float lr, XorNet xn, const std::vector<Gate> &tdata) {
  auto init_cost = cost(xn, tdata);
  float saved_weight = 0.0;
  XorNet res{xn};

  saved_weight = xn.nand_n.weights[0];
  xn.nand_n.weights[0] += eps;
  res.nand_n.weights[0] = (cost(xn, tdata) - init_cost)/eps;
  xn.nand_n.weights[0] = saved_weight;

  saved_weight = xn.nand_n.weights[1];
  xn.nand_n.weights[1] += eps;
  res.nand_n.weights[1] = (cost(xn, tdata) - init_cost)/eps;
  xn.nand_n.weights[1] = saved_weight;

  saved_weight = xn.nand_n.bias;
  xn.nand_n.bias += eps;
  res.nand_n.bias = (cost(xn, tdata) - init_cost)/eps;
  xn.nand_n.bias = saved_weight;

  saved_weight = xn.or_n.weights[0];
  xn.or_n.weights[0] += eps;
  res.or_n.weights[0] = (cost(xn, tdata) - init_cost)/eps;
  xn.or_n.weights[0] = saved_weight;

  saved_weight = xn.or_n.weights[1];
  xn.or_n.weights[1] += eps;
  res.or_n.weights[1] = (cost(xn, tdata) - init_cost)/eps;
  xn.or_n.weights[1] = saved_weight;

  saved_weight = xn.or_n.bias;
  xn.or_n.bias += eps;
  res.or_n.bias = (cost(xn, tdata) - init_cost)/eps;
  xn.or_n.bias = saved_weight;

  saved_weight = xn.and_n.weights[0];
  xn.and_n.weights[0] += eps;
  res.and_n.weights[0] = (cost(xn, tdata) - init_cost)/eps;
  xn.and_n.weights[0] = saved_weight;

  saved_weight = xn.and_n.weights[1];
  xn.and_n.weights[1] += eps;
  res.and_n.weights[1] = (cost(xn, tdata) - init_cost)/eps;
  xn.and_n.weights[1] = saved_weight;

  saved_weight = xn.or_n.bias;
  xn.and_n.bias += eps;
  res.and_n.bias = (cost(xn, tdata) - init_cost)/eps;
  xn.and_n.bias = saved_weight;

  xn.nand_n.weights[0] -= res.nand_n.weights[0]*lr;
  xn.nand_n.weights[1] -= res.nand_n.weights[1]*lr;
  xn.nand_n.bias -= res.nand_n.bias*lr;
  xn.or_n.weights[0] -= res.or_n.weights[0]*lr;
  xn.or_n.weights[1] -= res.or_n.weights[1]*lr;
  xn.or_n.bias -= res.or_n.bias*lr;
  xn.and_n.weights[0] -= res.and_n.weights[0]*lr;
  xn.and_n.weights[1] -= res.and_n.weights[1]*lr;
  xn.and_n.bias -= res.and_n.bias*lr;

  return xn;
}

float inference(XorNet &xn, float ai, float bi) {
  auto a = xn.nand_n.eval({ai, bi});
  auto b = xn.or_n.eval({ai, bi});
  auto y = xn.and_n.eval({a, b});

  return y;
}

int main() {
  /*std::srand(time(nullptr));*/
  std::srand(0x69);

  auto rfloat = [] (){
    uint64_t r = std::rand();

    return static_cast<float>(r)/static_cast<float>(RAND_MAX);
  };

  constexpr float eps = 1e-2;
  constexpr float rate = 1e-1;

  // xor = (a | b) & !(a & b)
  Neuron or_n{rfloat, rfloat, sigmoid_f};
  Neuron nand_n{rfloat, rfloat, sigmoid_f};
  Neuron and_n{rfloat, rfloat, sigmoid_f};
  XorNet xn (rfloat, rfloat, sigmoid_f);

  auto c = cost(xn, training_data);
  cout << xn.or_n << "\n" << xn.nand_n << "\n" << xn.and_n << "\n";
  cout << "cost: " << c << "\n";
  /*for (int i = 0; i < 20000; ++i) {*/
  size_t i = 0;
  while (c > 0.001) {
    ++i;
    xn = finite_diff(eps, rate, xn, training_data);
    c = cost(xn, training_data);
  }
  cout << c << ", i:" << i << "\n";
  cout << xn.or_n << "\n" << xn.nand_n << "\n" << xn.and_n << "\n";

  // for(int i = 0; i < 100; i++) {
  //   and_n = learn(eps, rate, and_n, training_data);
  //   c = cost(and_n, training_data);
  // }
  // cout << and_n << ", " << c << "\n";

  for(int i = 0; i < 2; ++i) {
    for (int j = 0; j < 2; ++j) {
      /*cout << i << " | " << j << " || " << and_n.eval({(float)i, (float)j}) << "\n"; */
      auto res = forward(xn, i, j);
      cout << i << " | " << j << " || " << std::roundf(res) << ", " << res << "\n";
    }
  }


  return 0;
}
