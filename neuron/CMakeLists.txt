# https://cliutils.gitlab.io/modern-cmake/

cmake_minimum_required(VERSION 3.7)

set(PROJECT_NAME simple_ml)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_EXTENSIONS OFF)

set(CMAKE_BUILD_TYPE Debug)
set(CMAKE_EXPORT_COMPILE_COMMANDS ON)

if (CMAKE_CXX_COMPILER_ID STREQUAL "GNU")
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -Werror -Wextra -Wconversion")
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wsign-conversion -Wno-long-long")
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -pedantic -pedantic-erros")
endif()

project(${PROJECT_NAME} VERSION 1.0 LANGUAGES CXX)

# maybe needed for future visualization
# find_package(SDL2 REQUIRED)
# include_directories(${SDL2_INCLUDE_DIRS})

file(GLOB SOURCES 
    src/neuron.cpp
)

add_executable(${PROJECT_NAME} 
    src/main.cpp
    ${SOURCES}
)

